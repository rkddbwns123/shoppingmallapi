package com.kyjg.shoppingmallapi.controller;

import com.kyjg.shoppingmallapi.model.*;
import com.kyjg.shoppingmallapi.service.BannerInfoService;
import com.kyjg.shoppingmallapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "배너 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/banner/info")
public class BannerInfoController {
    private final BannerInfoService bannerInfoService;

    @ApiOperation(value = "배너 정보 등록")
    @PostMapping("/new")
    public CommonResult setBannerInfo(@RequestBody @Valid BannerInfoRequest request) {
        bannerInfoService.setBannerInfo(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "배너 목록")
    @GetMapping("/use")
    public ListResult<String> getBannerInfo() {
        return ResponseService.getListResult(bannerInfoService.getBannerInfo(), true);
    }

    @ApiOperation(value = "배너 정보 수정")
    @PutMapping("/update/id/{id}")
    public CommonResult putBannerInfo(@PathVariable long id, @RequestBody @Valid BannerInfoUpdateRequest request) {
        bannerInfoService.putBannerInfo(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "배너 정보 삭제")
    @DeleteMapping("/remove/id/{id}")
    public CommonResult delBannerInfo(@PathVariable long id) {
        bannerInfoService.delBannerInfo(id);

        return ResponseService.getSuccessResult();
    }
}
