package com.kyjg.shoppingmallapi.controller;

import com.kyjg.shoppingmallapi.model.LoginRequest;
import com.kyjg.shoppingmallapi.model.LoginResponse;
import com.kyjg.shoppingmallapi.model.SingleResult;
import com.kyjg.shoppingmallapi.service.MemberService;
import com.kyjg.shoppingmallapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "로그인")
    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest request) {
        return ResponseService.getSingleResult(memberService.doLogin(request));
    }
}
