package com.kyjg.shoppingmallapi.controller;

import com.kyjg.shoppingmallapi.entity.ProductInfo;
import com.kyjg.shoppingmallapi.model.CommonResult;
import com.kyjg.shoppingmallapi.model.ListResult;
import com.kyjg.shoppingmallapi.model.ShoppingBasketItem;
import com.kyjg.shoppingmallapi.model.ShoppingBasketRequest;
import com.kyjg.shoppingmallapi.service.ProductInfoService;
import com.kyjg.shoppingmallapi.service.ResponseService;
import com.kyjg.shoppingmallapi.service.ShoppingBasketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Api(tags = "장바구니 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/shopping/basket")
public class ShoppingBasketController {
    private final ProductInfoService productInfoService;
    private final ShoppingBasketService shoppingBasketService;

    @ApiOperation(value = "장바구니 등록")
    @PostMapping("/cart")
    public CommonResult setShoppingBasket(@RequestBody @Valid ShoppingBasketRequest request) {
        ProductInfo productInfo = productInfoService.getData(request.getProductId());
        shoppingBasketService.setShoppingBasket(request.getMemberId(), productInfo);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "장바구니 목록")
    @GetMapping("/all/member-id/{memberId}")
    public ListResult<ShoppingBasketItem> getShoppingBasket(@PathVariable long memberId) {
        return ResponseService.getListResult(shoppingBasketService.getShoppingBasket(memberId), true);
    }

    @ApiOperation(value = "장바구니 삭제")
    @DeleteMapping("/remove/shopping-id/{shoppingId}")
    public CommonResult delShoppingBasket(@PathVariable long shoppingId) {
        shoppingBasketService.delShoppingBasket(shoppingId);

        return ResponseService.getSuccessResult();
    }
}
