package com.kyjg.shoppingmallapi.controller;

import com.kyjg.shoppingmallapi.enums.Category;
import com.kyjg.shoppingmallapi.model.*;
import com.kyjg.shoppingmallapi.service.ListConvertService;
import com.kyjg.shoppingmallapi.service.ProductInfoService;
import com.kyjg.shoppingmallapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "상품 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product/info")
public class ProductInfoController {
    private final ProductInfoService productInfoService;

    @ApiOperation(value = "상품 정보 등록")
    @PostMapping("/data")
    public CommonResult setProductInfo(@RequestBody @Valid ProductInfoRequest request) {
        productInfoService.setProductInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "상품 목록")
    @GetMapping("/category")
    public ListResult<ProductInfoItem> getProductInfo(@RequestParam Category category) {
        return ResponseService.getListResult(productInfoService.getProductInfo(category), true);
    }

    @ApiOperation(value = "상품 정보 수정")
    @PutMapping("/update/id/{id}")
    public CommonResult putProductInfo(@PathVariable long id, @RequestBody @Valid ProductInfoUpdateRequest request) {
        productInfoService.putProductInfo(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "상품 정보 삭제")
    @DeleteMapping("/remove/id/{id}")
    public CommonResult delProductInfo(@PathVariable long id) {
        productInfoService.delProductInfo(id);

        return ResponseService.getSuccessResult();
    }
}
