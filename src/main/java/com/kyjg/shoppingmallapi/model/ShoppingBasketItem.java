package com.kyjg.shoppingmallapi.model;

import com.kyjg.shoppingmallapi.entity.ShoppingBasket;
import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ShoppingBasketItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "이미지 주소")
    private String imageAddress;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "카테고리명")
    private String categoryName;

    @ApiModelProperty(notes = "원 가격")
    private Integer originPrice;

    @ApiModelProperty(notes = "할인 가격")
    private Integer disCountPrice;

    private ShoppingBasketItem(ShoppingBasketItemBuilder builder) {
        this.id = builder.id;
        this.imageAddress = builder.imageAddress;
        this.productName = builder.productName;
        this.categoryName = builder.categoryName;
        this.originPrice = builder.originPrice;
        this.disCountPrice = builder.disCountPrice;
    }

    public static class ShoppingBasketItemBuilder implements CommonModelBuilder<ShoppingBasketItem> {
        private final Long id;
        private final String imageAddress;
        private final String productName;
        private final String categoryName;
        private final Integer originPrice;
        private final Integer disCountPrice;

        public ShoppingBasketItemBuilder(ShoppingBasket shoppingBasket) {
            this.id = shoppingBasket.getProductInfo().getId();
            this.imageAddress = shoppingBasket.getProductInfo().getImageAddress();
            this.productName = shoppingBasket.getProductInfo().getProductName();
            this.categoryName = shoppingBasket.getProductInfo().getCategory().getCategoryName();
            this.originPrice = shoppingBasket.getProductInfo().getOriginPrice();
            this.disCountPrice = shoppingBasket.getProductInfo().getDisCountPrice();
        }

        @Override
        public ShoppingBasketItem build() {
            return new ShoppingBasketItem(this);
        }
    }


}
