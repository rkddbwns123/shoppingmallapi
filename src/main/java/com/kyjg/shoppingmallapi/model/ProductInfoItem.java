package com.kyjg.shoppingmallapi.model;

import com.kyjg.shoppingmallapi.entity.ProductInfo;
import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductInfoItem {
    private Long id;

    private String productName;

    private String imageAddress;

    private String categoryName;

    private String originPrice;

    private String disCountPrice;

    private ProductInfoItem(ProductInfoItemBuilder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.imageAddress = builder.imageAddress;
        this.categoryName = builder.categoryName;
        this.originPrice = builder.originPrice;;
        this.disCountPrice = builder.disCountPrice;

    }

    public static class ProductInfoItemBuilder implements CommonModelBuilder<ProductInfoItem> {

        private final Long id;
        private final String productName;
        private final String imageAddress;
        private final String categoryName;
        private final String originPrice;
        private final String disCountPrice;

        public ProductInfoItemBuilder(ProductInfo info) {
            this.id = info.getId();
            this.productName = info.getProductName();
            this.imageAddress = info.getImageAddress();
            this.categoryName = info.getCategory().getCategoryName();
            this.originPrice = info.getOriginPrice() + "￦";
            this.disCountPrice = info.getDisCountPrice() + "￦";
        }
        @Override
        public ProductInfoItem build() {
            return new ProductInfoItem(this);
        }
    }
}
