package com.kyjg.shoppingmallapi.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BannerInfoUpdateRequest {
    @NotNull
    @Length(min = 1, max = 200)
    private String bannerImageAddress;

    @NotNull
    private Boolean isUse;
}
