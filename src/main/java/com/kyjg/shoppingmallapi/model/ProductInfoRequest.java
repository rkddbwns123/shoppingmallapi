package com.kyjg.shoppingmallapi.model;

import com.kyjg.shoppingmallapi.enums.Category;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductInfoRequest {
    @NotNull
    @Length(min = 1, max = 200)
    private String imageAddress;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Category category;
    @NotNull
    @Length(min = 1, max = 50)
    private String productName;
    @NotNull
    @Min(value = 1)
    @Max(value = 100000000)
    private Integer originPrice;
    @NotNull
    @Min(value = 1)
    @Max(value = 100000000)
    private Integer disCountPrice;
}
