package com.kyjg.shoppingmallapi.model;

import com.kyjg.shoppingmallapi.entity.Member;
import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private Long id;

    private String name;

    private LoginResponse(LoginResponseBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {

        private final Long id;

        private final String name;

        public LoginResponseBuilder(Member member) {
            this.id = member.getId();
            this.name = member.getName();
        }
        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
