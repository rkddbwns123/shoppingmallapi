package com.kyjg.shoppingmallapi.model;

import com.kyjg.shoppingmallapi.entity.BannerInfo;
import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerInfoItem {
    private Long id;

    private String bannerImageAddress;

    private String isUse;

    private BannerInfoItem(BannerInfoItemBuilder builder) {
        this.id = builder.id;
        this.bannerImageAddress = builder.bannerImageAddress;
        this.isUse = builder.isUse;

    }

    public static class BannerInfoItemBuilder implements CommonModelBuilder<BannerInfoItem> {

        private final Long id;
        private final String bannerImageAddress;
        private final String isUse;

        public BannerInfoItemBuilder(BannerInfo info) {
            this.id = info.getId();
            this.bannerImageAddress = info.getBannerImageAddress();
            this.isUse = info.getIsUse() ? "사용 중" : "미사용";
        }

        @Override
        public BannerInfoItem build() {
            return new BannerInfoItem(this);
        }
    }
}
