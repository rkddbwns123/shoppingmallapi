package com.kyjg.shoppingmallapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ShoppingBasketRequest {
    @NotNull
    private Long memberId;
    @NotNull
    private Long productId;
}
