package com.kyjg.shoppingmallapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    APPLIANCE("IT, 가전제품"),
    FOOD("식품"),
    TOY("장난감");

    private final String categoryName;
}
