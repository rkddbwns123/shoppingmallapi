package com.kyjg.shoppingmallapi.interfaces;

import com.kyjg.shoppingmallapi.entity.ProductInfo;
import com.kyjg.shoppingmallapi.enums.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductInfoRepository extends JpaRepository<ProductInfo, Long> {
    List<ProductInfo> findByCategory(Category category);
}
