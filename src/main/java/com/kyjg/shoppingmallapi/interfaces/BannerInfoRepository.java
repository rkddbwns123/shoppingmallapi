package com.kyjg.shoppingmallapi.interfaces;

import com.kyjg.shoppingmallapi.entity.BannerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerInfoRepository extends JpaRepository<BannerInfo, Long> {
    List<BannerInfo> findAllByIsUseOrderByIdAsc(boolean isUse);
}
