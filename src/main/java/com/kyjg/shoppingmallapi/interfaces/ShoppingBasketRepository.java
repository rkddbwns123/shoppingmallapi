package com.kyjg.shoppingmallapi.interfaces;

import com.kyjg.shoppingmallapi.entity.ShoppingBasket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShoppingBasketRepository extends JpaRepository<ShoppingBasket, Long> {
    List<ShoppingBasket> findAllByIdOrderByIdAsc(long id);
}
