package com.kyjg.shoppingmallapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
