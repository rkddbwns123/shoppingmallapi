package com.kyjg.shoppingmallapi.entity;

import com.kyjg.shoppingmallapi.enums.Category;
import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import com.kyjg.shoppingmallapi.model.ProductInfoRequest;
import com.kyjg.shoppingmallapi.model.ProductInfoUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @Column(nullable = false, length = 200)
    @ApiModelProperty(notes = "이미지 주소")
    private String imageAddress;

    @ApiModelProperty(notes = "카테고리")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Category category;

    @Column(nullable = false, length = 50)
    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "원래 가격")
    @Column(nullable = false)
    private Integer originPrice;

    @ApiModelProperty(notes = "할인 가격")
    @Column(nullable = false)
    private Integer disCountPrice;

    public void putProductInfo(ProductInfoUpdateRequest request) {
        this.imageAddress = request.getImageAddress();
        this.productName = request.getProductName();
        this.originPrice = request.getOriginPrice();
        this.disCountPrice = request.getDisCountPrice();
    }

    private ProductInfo(ProductInfoBuilder builder) {
        this.imageAddress = builder.imageAddress;
        this.category = builder.category;
        this.productName = builder.productName;
        this.originPrice = builder.originPrice;
        this.disCountPrice = builder.disCountPrice;

    }

    public static class ProductInfoBuilder implements CommonModelBuilder<ProductInfo> {

        private final String imageAddress;
        private final Category category;
        private final String productName;
        private final Integer originPrice;
        private final Integer disCountPrice;

        public ProductInfoBuilder(ProductInfoRequest request) {
            this.imageAddress = request.getImageAddress();
            this.category = request.getCategory();
            this.productName = request.getProductName();
            this.originPrice = request.getOriginPrice();
            this.disCountPrice = request.getDisCountPrice();
        }

        @Override
        public ProductInfo build() {
            return new ProductInfo(this);
        }
    }
}
