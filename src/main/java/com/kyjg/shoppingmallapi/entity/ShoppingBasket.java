package com.kyjg.shoppingmallapi.entity;

import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import com.kyjg.shoppingmallapi.model.ShoppingBasketRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ShoppingBasket {

    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "회원 ID")
    @Column(nullable = false)
    private Long memberId;

    @ApiModelProperty(notes = "상품 정보")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, columnDefinition = "productInfoId")
    private ProductInfo productInfo;

    private ShoppingBasket(ShoppingBasketBuilder builder) {
        this.memberId = builder.memberId;
        this.productInfo = builder.productInfo;
    }

    public static class ShoppingBasketBuilder implements CommonModelBuilder<ShoppingBasket> {

        private final Long memberId;
        private final ProductInfo productInfo;

        public ShoppingBasketBuilder(long memberId, ProductInfo productInfo) {
            this.memberId = memberId;
            this.productInfo = productInfo;
        }

        @Override
        public ShoppingBasket build() {
            return new ShoppingBasket(this);
        }
    }
}
