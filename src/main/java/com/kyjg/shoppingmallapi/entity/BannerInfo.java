package com.kyjg.shoppingmallapi.entity;

import com.kyjg.shoppingmallapi.interfaces.CommonModelBuilder;
import com.kyjg.shoppingmallapi.model.BannerInfoRequest;
import com.kyjg.shoppingmallapi.model.BannerInfoUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerInfo {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "배너 이미지 주소")
    @Column(nullable = false, length = 200)
    private String bannerImageAddress;

    @ApiModelProperty(notes = "사용 여부")
    @Column(nullable = false)
    private Boolean isUse;

    public void putBannerInfo(BannerInfoUpdateRequest request) {
        this.bannerImageAddress = request.getBannerImageAddress();
        this.isUse = request.getIsUse();
    }

    private BannerInfo(BannerInfoBuilder builder) {
        this.bannerImageAddress = builder.bannerImageAddress;
        this.isUse = builder.isUse;
    }

    public static class BannerInfoBuilder implements CommonModelBuilder<BannerInfo> {

        private final String bannerImageAddress;
        private final Boolean isUse;

        public BannerInfoBuilder(BannerInfoRequest request) {
            this.bannerImageAddress = request.getBannerImageAddress();
            this.isUse = false;
        }

        @Override
        public BannerInfo build() {
            return new BannerInfo(this);
        }
    }
}
