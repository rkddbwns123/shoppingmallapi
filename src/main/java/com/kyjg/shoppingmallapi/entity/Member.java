package com.kyjg.shoppingmallapi.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "고객 명")
    @Column(nullable = false, length = 20)
    private String name;

    @ApiModelProperty(notes = "아이디")
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @ApiModelProperty(notes = "비밀번호")
    @Column(nullable = false, length = 100)
    private String password;
}
