package com.kyjg.shoppingmallapi.service;

import com.kyjg.shoppingmallapi.entity.ProductInfo;
import com.kyjg.shoppingmallapi.entity.ShoppingBasket;
import com.kyjg.shoppingmallapi.interfaces.ShoppingBasketRepository;
import com.kyjg.shoppingmallapi.model.ListResult;
import com.kyjg.shoppingmallapi.model.ShoppingBasketItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ShoppingBasketService {
    private final ShoppingBasketRepository shoppingBasketRepository;
    public void setShoppingBasket(long memberId, ProductInfo productInfo) {
        ShoppingBasket addData = new ShoppingBasket.ShoppingBasketBuilder(memberId, productInfo).build();

        shoppingBasketRepository.save(addData);
    }

    public ListResult<ShoppingBasketItem> getShoppingBasket(long memberId) {
        List<ShoppingBasket> originList = shoppingBasketRepository.findAllByIdOrderByIdAsc(memberId);

        List<ShoppingBasketItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ShoppingBasketItem.ShoppingBasketItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void delShoppingBasket(long shoppingId) {
        shoppingBasketRepository.deleteById(shoppingId);
    }
}
