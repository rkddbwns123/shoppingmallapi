package com.kyjg.shoppingmallapi.service;

import com.kyjg.shoppingmallapi.entity.BannerInfo;
import com.kyjg.shoppingmallapi.interfaces.BannerInfoRepository;
import com.kyjg.shoppingmallapi.model.BannerInfoItem;
import com.kyjg.shoppingmallapi.model.BannerInfoRequest;
import com.kyjg.shoppingmallapi.model.BannerInfoUpdateRequest;
import com.kyjg.shoppingmallapi.model.ListResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BannerInfoService {
    private final BannerInfoRepository bannerInfoRepository;

    public void setBannerInfo(BannerInfoRequest request) {
        BannerInfo addData = new BannerInfo.BannerInfoBuilder(request).build();

        bannerInfoRepository.save(addData);
    }

    public ListResult<String> getBannerInfo() {
        List<BannerInfo> originList = bannerInfoRepository.findAllByIsUseOrderByIdAsc(true);

        List<String> result = new LinkedList<>();

        for (BannerInfo item : originList) {
            result.add(item.getBannerImageAddress());
        }
        return ListConvertService.settingResult(result);
    }

    public void putBannerInfo(long id, BannerInfoUpdateRequest request) {
        BannerInfo originData = bannerInfoRepository.findById(id).orElseThrow();

        originData.putBannerInfo(request);

        bannerInfoRepository.save(originData);
    }

    public void delBannerInfo(long id) {
        bannerInfoRepository.deleteById(id);
    }
}
