package com.kyjg.shoppingmallapi.service;

import com.kyjg.shoppingmallapi.entity.ProductInfo;
import com.kyjg.shoppingmallapi.enums.Category;
import com.kyjg.shoppingmallapi.interfaces.ProductInfoRepository;
import com.kyjg.shoppingmallapi.model.ListResult;
import com.kyjg.shoppingmallapi.model.ProductInfoItem;
import com.kyjg.shoppingmallapi.model.ProductInfoRequest;
import com.kyjg.shoppingmallapi.model.ProductInfoUpdateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductInfoService {
    private final ProductInfoRepository productInfoRepository;

    public void setProductInfo(ProductInfoRequest request) {
        ProductInfo addData = new ProductInfo.ProductInfoBuilder(request).build();

        productInfoRepository.save(addData);
    }

    public ListResult<ProductInfoItem> getProductInfo(Category category) {
        List<ProductInfo> originList = productInfoRepository.findByCategory(category);

        List<ProductInfoItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ProductInfoItem.ProductInfoItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putProductInfo(long id, ProductInfoUpdateRequest request) {
        ProductInfo originData = productInfoRepository.findById(id).orElseThrow();

        originData.putProductInfo(request);

        productInfoRepository.save(originData);
    }

    public void delProductInfo(long id) {
        productInfoRepository.deleteById(id);
    }

    public ProductInfo getData(long productId) {
        return productInfoRepository.findById(productId).orElseThrow();
    }
}
