package com.kyjg.shoppingmallapi.service;

import com.kyjg.shoppingmallapi.entity.Member;
import com.kyjg.shoppingmallapi.exception.CMissingDataException;
import com.kyjg.shoppingmallapi.interfaces.MemberRepository;
import com.kyjg.shoppingmallapi.model.LoginRequest;
import com.kyjg.shoppingmallapi.model.LoginResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public LoginResponse doLogin(LoginRequest request) {
        Member member = memberRepository.findByUsername(request.getUsername()).orElseThrow(CMissingDataException::new);

        if (!member.getPassword().equals(request.getPassword())) throw new CMissingDataException();

        return new LoginResponse.LoginResponseBuilder(member).build();
    }
}
