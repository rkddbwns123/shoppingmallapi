package com.kyjg.shoppingmallapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingMallApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingMallApiApplication.class, args);
	}

}
